import { createApp } from 'vue';
import { createPinia } from 'pinia';

import App from './App.vue';
import router from './router';

import CBtn from './components/globals/BtnGlobal.vue';

import './assets/tailwind.css';

const app = createApp(App);

app.component('CBtn', CBtn);

app.use(createPinia());
app.use(router);

app.mount('#app');
