/*eslint-env node */

module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        brand: {
          DEFAULT: '#fb8c00',
          text: '#171986',
        },
      },
    },
  },
  plugins: [],
};
